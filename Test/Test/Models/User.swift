//
//  User.swift
//  Test
//
//  Created by Chính Trình Quang on 7/16/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
class User {
    var name: String?
    var password: String?
    var avatarURL: String?
    init(name: String, password: String, avatarURL: String) {
        self.name = name
        self.password = password
        self.avatarURL = avatarURL
    }
    init(){
        
    }
    
}
