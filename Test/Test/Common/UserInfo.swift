//
//  UserInfo.swift
//  Test
//
//  Created by Chính Trình Quang on 7/16/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
class UserInfo{
    private static var userInfo = User()
    public static var shareInstance = UserInfo()
    func getName() -> String {
        return UserInfo.userInfo.name ?? ""
    }
    func setUserData(newInfo : User) {
        UserInfo.userInfo.name = newInfo.name
        UserInfo.userInfo.password = newInfo.password
        UserInfo.userInfo.avatarURL = newInfo.avatarURL
    }
    func saveLocal() {
        UserDefaults.standard.set(UserInfo.userInfo.name, forKey: "username")
        
        UserDefaults.standard.set(UserInfo.userInfo.password, forKey: "password")
        UserDefaults.standard.set(UserInfo.userInfo.avatarURL, forKey: "avatarURL")
        
    }
    func fetchFromLocal() -> User {
        if let username = UserDefaults.standard.string(forKey: "username"), let password = UserDefaults.standard.string(forKey: "password"), let avatarURL = UserDefaults.standard.string( forKey: "avatarURL"){
            print(username)
             return User(name: username, password: password, avatarURL: avatarURL)
        }
        else{
            return User()
        }
       
    }
    func deleteFromLocal() -> Void {
        if let appDomain = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: appDomain)
        }
        
    }
    
    
}
