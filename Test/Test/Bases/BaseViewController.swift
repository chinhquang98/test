	//
//  BaseViewController.swift
//  Test
//
//  Created by Chính Trình Quang on 7/16/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupLayouts()
        // Do any additional setup after loading the view.
    }
    
    func setupViews(){
        view.backgroundColor = .black
    }
    func setupLayouts() {
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
