//
//  TextField.swift
//  Test
//
//  Created by Chính Trình Quang on 7/16/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation

import UIKit
class TextField: UITextField {
    
    let padding = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
}
extension TextField{
    func unenableEditting(){
        self.isEnabled = false
        self.isUserInteractionEnabled = false
    }
}
