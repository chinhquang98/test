//
//  ViewController.swift
//  Test
//
//  Created by Chính Trình Quang on 7/16/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

import SDWebImage

class FirstViewController: BaseViewController {
    var viewSize  = UIScreen.main.bounds
    var user : User?{
        didSet{
            usernameTextField.text = self.user?.name
            passwordTextField.text = self.user?.password
            
            
            avatarImage.sd_setImage(with: URL(string: self.user!.avatarURL ?? ""), placeholderImage: UIImage(named: "avatar-placeholder"))
        }
    }
    lazy var avatarImage: UIImageView = {
        let image  = UIImageView(frame: .zero)
        image.contentMode = .scaleAspectFit
        image.layer.masksToBounds = true
        image.layer.borderWidth = 10
        image.layer.borderColor = UIColor(white: 1, alpha: 0.5).cgColor
        image.translatesAutoresizingMaskIntoConstraints = false
        
        return image
    }()
    
    @objc func presentView (_ recognizer : UIGestureRecognizer){
        print("hello")
        
    
        //UserInfo.shareInstance.saveLocal()
        let secondVC = SecondViewController()
        secondVC.delegate = self
        self.present(secondVC, animated: true, completion: nil)
    }
    lazy var usernameTextField : TextField = {
        let textfield  = TextField()
        textfield.translatesAutoresizingMaskIntoConstraints = false
        
        textfield.textColor = .white
        textfield.tintColor = .white
        textfield.layer.borderColor = UIColor.white.cgColor
        textfield.attributedPlaceholder = NSAttributedString(string: "Enter your email",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1, alpha: 0.5)])
        textfield.layer.borderWidth = 1
        textfield.unenableEditting()
        return textfield
    }()
    lazy var passwordTextField : TextField = {
        let textfield  = TextField()
        textfield.translatesAutoresizingMaskIntoConstraints = false
        textfield.isSecureTextEntry = true
        textfield.textColor = .white
        textfield.tintColor = .white
        textfield.layer.borderColor = UIColor.white.cgColor
        textfield.attributedPlaceholder = NSAttributedString(string: "Enter your password",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1, alpha: 0.5)])
        textfield.layer.borderWidth = 1
        textfield.unenableEditting()
       
        return textfield
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        user = UserInfo.shareInstance.fetchFromLocal()
        
    }
    override func setupViews() {
        super.setupViews()
        view.addSubview(avatarImage)
        view.addSubview(usernameTextField)
        view.addSubview(passwordTextField)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(presentView(_:)))
        avatarImage.isUserInteractionEnabled = true
        avatarImage.addGestureRecognizer(recognizer)
        
        
    }
    override func setupLayouts() {
        super.setupLayouts()
        avatarImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        avatarImage.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -viewSize.height / 4).isActive = true
        avatarImage.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.6).isActive = true
        avatarImage.heightAnchor.constraint(equalTo: avatarImage.widthAnchor).isActive = true
        
        
        usernameTextField.topAnchor.constraint(equalTo: avatarImage.bottomAnchor, constant: viewSize.height * 0.18).isActive = true
        usernameTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        usernameTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.91).isActive = true
        usernameTextField.heightAnchor.constraint(equalTo: usernameTextField.widthAnchor,multiplier: 0.14,constant: 0).isActive = true
        
        
        
        passwordTextField.topAnchor.constraint(equalTo: usernameTextField.bottomAnchor, constant: view.frame.height * 0.03).isActive = true
        passwordTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        passwordTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.91).isActive = true
        passwordTextField.heightAnchor.constraint(equalTo: usernameTextField.widthAnchor,multiplier: 0.14,constant: 0).isActive = true
        
        view.layoutIfNeeded()
        avatarImage.layer.cornerRadius = avatarImage.frame.height/2
    }
    
}

extension FirstViewController: SecondViewControllerDelegate{
    func saveInfo(_ user: User?) {
        self.user = user
    }
    
    
}
